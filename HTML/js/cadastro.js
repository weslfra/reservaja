$(document).ready(function() {

    var guestAmount = $('#guestNo');
  
    $('#cnt-up').click(function() {
      guestAmount.val(Math.min(parseInt($('#guestNo').val()) + 1, 20));
    });
    $('#cnt-down').click(function() {
      guestAmount.val(Math.max(parseInt($('#guestNo').val()) - 1, 1));
    });
  
    $('.btn').click(function() {
      let dados = {};
      let formulario = $("form");

      formulario.find("input").each(
          function(unusedIndex, child) {
            dados[child.name] = child.value; });

      console.log(dados);

      if(formulario.attr("name") == "cadastroCliente"){
        url = "http://localhost:8080/cliente";
      }
      else if(formulario.attr("name") == "cadastroParceiro"){
        url = "http://localhost:8080/parceiro";
      }

      fetch(url,{
            method: "post",
            body: JSON.stringify(dados),
             headers: {
              'Content-Type': 'application/json'
      }}).then(resposta => {
        window.alert("Cadastro efetuado com sucesso");
      }).catch(err => {
        window.alert("Erro no preenchimento do formulário");
      });
  
      // var $btn = $('.btn');
  
      // $btn.toggleClass('Cadastrado');
      // $('.diamond').toggleClass('windup');
      // $('form').slideToggle(300);
      // $('.linkbox').toggle(200);
  
      // if ($btn.text() === "Cadastrado") {
      //   $btn.text("Cadastrado!");
      // } else {
      //   $btn.text("Cadastrado!");
      // }
    });
  });