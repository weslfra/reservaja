package com.reservaja.reservaja.clientes.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reservaja.reservaja.entidades.Cliente;
import com.reservaja.reservaja.entidades.Mesa;
import com.reservaja.reservaja.entidades.Parceiro;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.Papel;
import com.reservaja.reservaja.enums.StatusMesa;
import com.reservaja.reservaja.repository.MesaRepository;
import com.reservaja.reservaja.repository.ParceiroRepository;
import com.reservaja.reservaja.service.TokenService;

@Controller
public class MesaController {

	@Autowired
	MesaRepository mesaRepository;
	
	@Autowired
	ParceiroRepository parceiroRepository;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/mesas", method=RequestMethod.GET)
	@ResponseBody 
	public Iterable<Mesa> getMesas(@RequestParam("cnpj") String cnpjParceiro) {	
		System.out.println(cnpjParceiro);
		Optional <Parceiro> parceiroQuery = parceiroRepository.findByCnpj(cnpjParceiro);
		return mesaRepository.findAllByParceiroAndStatusMesa(parceiroQuery.get(), StatusMesa.DISPONIVEL);
	}
	
	@RequestMapping(path="/mesa", method=RequestMethod.POST)
	@ResponseBody 
	public Mesa criarMesa(HttpServletRequest request, @RequestBody Mesa mesa) {
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		Usuario usuario = tokenService.verificar(token);
		
		if(usuario != null && usuario.getPapel().compareTo(Papel.PARCEIRO) == 0) {
			Parceiro parceiro = parceiroRepository.findByUsuario(usuario).get();
			
			mesa.setParceiro(parceiro);
			mesa.setStatusMesa(StatusMesa.DISPONIVEL);
			
			return mesaRepository.save(mesa);
		}
		
		return null;
	}
	

	
}
