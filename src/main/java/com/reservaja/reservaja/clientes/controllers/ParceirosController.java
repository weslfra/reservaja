package com.reservaja.reservaja.clientes.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.reservaja.reservaja.entidades.Cliente;
import com.reservaja.reservaja.entidades.Parceiro;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.Papel;
import com.reservaja.reservaja.repository.ParceiroRepository;
import com.reservaja.reservaja.repository.UsuarioRepository;
import com.reservaja.reservaja.service.PasswordService;

@RestController

//Mapeia as requisicoes de Local host:8080/parceiros_
@RequestMapping

public class ParceirosController {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PasswordService passwordService;
	
	@Autowired
	ParceiroRepository parceiroRepository;
	@RequestMapping(path="/parceiros", 
			method=RequestMethod.GET)


	@ResponseBody 
	public Iterable<Parceiro> getParceiros(){
		return parceiroRepository.findAll();
	}

	@RequestMapping(path="/parceiro", method=RequestMethod.POST)
	@ResponseBody
	public Parceiro createParceiro(@RequestBody Parceiro parceiro) {		
		Usuario usuario = parceiro.getUsuario();
		usuario.setSenha(passwordService.encode(usuario.getSenha()));
		usuario.setPapel(Papel.PARCEIRO);
		usuario = usuarioRepository.save(usuario);

		parceiro.setUsuario(usuario);

		return parceiroRepository.save(parceiro);
	}

	@RequestMapping(path="/parceiro/{email}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Parceiro> getParceiroByEmail(@PathVariable String email) {		
		Optional<Usuario> usuarioQuery = usuarioRepository.findByEmail(email);

		if(!usuarioQuery.isPresent()) {
			return null;
		}

		Usuario usuario = usuarioQuery.get();

		return parceiroRepository.findByUsuario(usuario);
	}
}

