package com.reservaja.reservaja.clientes.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.reservaja.reservaja.entidades.Mesa;
import com.reservaja.reservaja.entidades.Parceiro;
import com.reservaja.reservaja.entidades.Reserva;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.Papel;
import com.reservaja.reservaja.enums.StatusMesa;
import com.reservaja.reservaja.repository.MesaRepository;
import com.reservaja.reservaja.repository.ParceiroRepository;
import com.reservaja.reservaja.repository.ReservaRepository;
import com.reservaja.reservaja.repository.UsuarioRepository;
import com.reservaja.reservaja.service.TokenService;

@Controller
public class ReservaController {

	@Autowired
	ReservaRepository reservaRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	MesaRepository mesaRepository;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/reserva", method=RequestMethod.POST)

	//public Reserva efetuarReserva(HttpServletRequest request, @RequestBody Reserva reserva) {
	public @ResponseBody ResponseEntity<?> efetuarReserva(HttpServletRequest request, @RequestBody Reserva reserva) {
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		Usuario usuario = tokenService.verificar(token);
		
		if(usuario != null) {
			
			Optional <Mesa> mesaQuery = mesaRepository.findById(reserva.getMesa().getId());
			
			if(mesaQuery.get().getStatusMesa().compareTo(StatusMesa.DISPONIVEL) == 0)	
			 {
				reserva.setUsuario(usuario);
				marcaMesaOcupada(mesaQuery.get());
			
				reservaRepository.save(reserva);
				return ResponseEntity.ok(reserva);
			}
			else {
				return ResponseEntity.badRequest().build();
			}
		}
		
		return null;
	}
	
	public void marcaMesaOcupada(Mesa mesa) {
		mesa.setStatusMesa(StatusMesa.OCUPADA);
		
		mesaRepository.save(mesa);
	}
	
}

