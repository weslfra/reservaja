package com.reservaja.reservaja.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Cliente;
import com.reservaja.reservaja.entidades.Usuario;


public interface ClienteRepository extends CrudRepository<Cliente, String> {
	public Optional<Cliente> findByUsuario(Usuario usuario);
	
}
