package com.reservaja.reservaja.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Parceiro;
import com.reservaja.reservaja.entidades.Mesa;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.StatusMesa;



public interface MesaRepository extends CrudRepository<Mesa, Integer> {
	public Set<Mesa> findAllByParceiro(Parceiro parceiro);
	public Set<Mesa> findAllByParceiroAndStatusMesa(Parceiro parceiro, StatusMesa statusMesa);
	
}
