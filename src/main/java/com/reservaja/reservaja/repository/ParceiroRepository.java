package com.reservaja.reservaja.repository;

import java.util.Optional;

import org.hibernate.validator.constraints.br.CNPJ;
import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Cliente;
import com.reservaja.reservaja.entidades.Parceiro;
import com.reservaja.reservaja.entidades.Usuario;

public interface  ParceiroRepository extends CrudRepository<Parceiro, CNPJ> {
	public Optional<Parceiro> findByUsuario(Usuario usuario);
	public Optional<Parceiro> findByCnpj(String cnpj);
	
}
