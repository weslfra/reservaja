package com.reservaja.reservaja.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.reservaja.reservaja.entidades.Reserva;
import com.reservaja.reservaja.entidades.Usuario;



public interface ReservaRepository extends CrudRepository<Reserva, Integer> {
	public Optional<Reserva> findByUsuario(Usuario usuario);
	
}
