package com.reservaja.reservaja.service;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.reservaja.reservaja.entidades.Usuario;
import com.reservaja.reservaja.enums.Papel;

@Service
public class TokenService {
	String secret = "mastertech123";
	
	public String gerar(long idUsuario, String papel ) {
		try {
			Date dataExpirar = new Date();
			Calendar calendario = Calendar.getInstance();
			
			calendario.setTime(dataExpirar);
			calendario.add(Calendar.MINUTE, 30);
			
			dataExpirar = calendario.getTime();
			
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    return JWT.create()
		    		.withExpiresAt(dataExpirar)
		    		.withClaim("idUsuario", idUsuario)
		    		.withClaim("papel", papel)
		    		.sign(algorithm);
		} catch (Exception exception){
			exception.printStackTrace();
			
			return null;
		}
	}
	
	
	public Usuario verificar(String token) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    JWTVerifier verifier = JWT.require(algorithm).build();
		    DecodedJWT jwt = verifier.verify(token);
		    Usuario usuario = new Usuario();
		    usuario.setId(jwt.getClaim("idUsuario").asInt());
		    Papel papel = (Papel.valueOf(jwt.getClaim("papel").asString()));
		    
		    usuario.setPapel(papel);
		    
		    return usuario;
		} catch (Exception exception){
		    exception.printStackTrace();
			
			return null;
		}
	}
}